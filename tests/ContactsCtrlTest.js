describe('ContactsCtrl', function() {
    var fakeWindow = {
      location: {
        href: ''
      }
    }

    var fakeModal = {
        result: {
            then: function(confirmCallback, cancelCallback) {
                this.confirmCallBack = confirmCallback;
                this.cancelCallback = cancelCallback;
            }
        },
        close: function( item ) {
            this.result.confirmCallBack( item );
        },
        dismiss: function( type ) {
            this.result.cancelCallback( type );
        }
    };


    beforeEach(module('AddressBook'));
    beforeEach(inject(function($uibModal) {
        spyOn($uibModal, 'open').and.returnValue(fakeModal);
    }));
    var $controller;

    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
    }));

    describe('$scope.refresh', function() {
        it('Checking if refresh contacts is working fine', function() {
            var $scope = {};
            var row = 2;

            var contact = {
                id: 2,
                first_name: 'Barack',
                last_name: 'Obama',
                email: 'obama@yahoo.com',
                country: 'United States of America (the)'
            }
            var controller = $controller('ContactsCtrl', { $scope: $scope, $window: fakeWindow });
            $scope.contacts = {};
            $scope.newContact = {};
            $scope.submitted = null;
            $scope.refresh();
            expect($scope.contacts[row-1].first_name).toEqual(contact.first_name);
            expect($scope.contacts[row-1].last_name).toEqual(contact.last_name);
            expect($scope.contacts[row-1].email).toEqual(contact.email);
            expect($scope.contacts[row-1].country).toEqual(contact.country);
            expect($scope.submitted).toEqual(false);
            expect($scope.newContact.country).toEqual('Poland');
        });
    });

    describe('$scope.edit', function() {
        it('Checking if editing contact is working fine', function() {
            var $scope = {};
            var row = 1;

            var contact = {
                id: 1,
                first_name: 'Stefan1',
                last_name: 'Kowalski1',
                email: 'kowalski1@gmail.com',
                country: 'Poland'
            }
            var controller = $controller('ContactsCtrl', { $scope: $scope, $window: fakeWindow });
            $scope.contactsForm = {};
            $scope.contactsForm['first_name_' + row] = {};
            $scope.contactsForm['first_name_' + row].$valid = true;
            $scope.contactsForm['last_name_' + row] = {};
            $scope.contactsForm['last_name_' + row].$valid = true;
            $scope.contactsForm['email_' + row] = {};
            $scope.contactsForm['email_' + row].$valid = true;
            $scope.contactsForm['country_' + row] = {};
            $scope.contactsForm['country_' + row].$valid = true;
            $scope.edit(row, contact);
            $scope.refresh();
            expect($scope.contacts[row-1].first_name).toEqual(contact.first_name);
            expect($scope.contacts[row-1].last_name).toEqual(contact.last_name);
            expect($scope.contacts[row-1].email).toEqual(contact.email);
            expect($scope.contacts[row-1].country).toEqual(contact.country);
        });
    });

    describe('$scope.add', function() {
        it('Checking if adding contact is working fine', function() {
            var $scope = {};
            var row = 5;

            var contact = {
                first_name: 'Stefan1',
                last_name: 'Kowalski1',
                email: 'kowalski1@gmail.com',
                country: 'Poland'
            }
            var controller = $controller('ContactsCtrl', { $scope: $scope, $window: fakeWindow });
            $scope.contactForm = {};
            $scope.contactForm.$valid = true;
            $scope.add(contact);
            expect($scope.contacts[row-1].first_name).toEqual(contact.first_name);
            expect($scope.contacts[row-1].last_name).toEqual(contact.last_name);
            expect($scope.contacts[row-1].email).toEqual(contact.email);
            expect($scope.contacts[row-1].country).toEqual(contact.country);
        });
    });
    describe('$scope.delete', function() {
        it('Checking if deleting contact is working fine', function() {
            var $scope = {};
            var row = 5;

            var controller = $controller('ContactsCtrl', { $scope: $scope, $window: fakeWindow});
            $scope.contactForm = {};
            $scope.contactForm.$valid = true;
            $scope.delete(row);
            $scope.modalInstance.close();
            expect($scope.contacts[row-1]).toBeUndefined();
        });
    });
});