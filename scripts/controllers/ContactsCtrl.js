'use strict';
app.controller('ContactsCtrl', ['$scope', '$location', 'contactModel', '$window', '$uibModal', function($scope, $location, contactModel, $window, $uibModal){

    $scope.refresh = function () {
        $scope.contacts = contactModel.getAll();
        $scope.newContact = {};
        $scope.newContact.country = 'Poland';
        $scope.submitted = false;
    };

    $scope.edit = function (rowid, contact) {
        if ($scope.contactsForm['first_name_' + rowid].$valid && $scope.contactsForm['last_name_' + rowid].$valid &&
            $scope.contactsForm['email_' + rowid].$valid && $scope.contactsForm['country_' + rowid].$valid) {
            contactModel.update(contact.id, contact);
            $window.location.href = "/#/admin";
        }
    };

    $scope.delete = function (contactId) {

        $scope.modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/views/modal_window.html',
            controller: 'ModalWindowCtrl',
            size: 'sm',
            resolve: {
                message: function () {
                    return 'Are you sure you want to delete contact?';
                }
            }
        });

        $scope.modalInstance.result.then(function () {
            contactModel.delete(contactId);
            $scope.refresh();
            $window.location.href = "/#/admin";
        }, function () {
            console.log('Modal dismissed');
        });
    };

    $scope.add = function (contact) {
        $scope.submitted = true;
        if ($scope.contactForm.$valid) {
            contactModel.add(contact);
            $scope.refresh();
            $window.location.href = "/#/admin";
        }
    };
    $scope.goToList = function () {
        $location.path('/');
    };
    $scope.goToAdmin = function () {
        $location.path('/admin');
    };
    $scope.refresh();

}]);