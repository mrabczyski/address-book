'use strict';
var app = angular.module("AddressBook", ['ngRoute', 'ngStorage', 'ngMessages', 'puigcerber.countryPicker', 'ui.bootstrap', 'ngAnimate'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'views/list.html'
            }).
            when('/admin', {
                templateUrl: 'views/admin.html'
            }).
            otherwise({
                redirectTo: '/'
            });
    }]);
app.run(function ($rootScope, $location, $localStorage, $window) {

});