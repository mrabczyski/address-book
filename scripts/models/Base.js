'use strict';
var BaseModel = function ($localStorage, modelName) {

    this.Initialize = function (list) {
        if($localStorage[modelName] === undefined) {
            this.data = list;
        } else {
            this.data = $localStorage[modelName];
        }
    };

    this.getAll = function () {
        return angular.copy(this.data);
    };

    this.add = function (obj) {
        var contactsNo = this.data.length;
        obj.id = (contactsNo===0)? 0 : (this.data[contactsNo-1].id+1);
        this.data.push(obj);
        $localStorage[modelName] = this.data;
        return true;
    };

    this.delete = function (id) {
        var notDeleted = this.data.filter(function(item) {
            if(item.id != id) return item;
        });
        this.data = notDeleted;
        $localStorage[modelName] = notDeleted;
        return true;
    };

    this.update = function (id, obj) {
        var updated = this.data.filter(function(item) {
            if(item.id == id) return item;
        });
        var i = this.data.indexOf(updated[0]);
        this.data[i] = obj;
        $localStorage[modelName] = this.data;
        return true;
    };
};