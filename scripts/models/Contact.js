'use strict';
app.factory('contactModel', function ($localStorage, $q) {
    var model = 'contacts';
    var ContactModel = new BaseModel($localStorage, model, $q);

    var contacts = [
        {
            id: 1,
            first_name: 'Stefan',
            last_name: 'Kowalski',
            email: 'kowalski@gmail.com',
            country: 'Poland'
        },
        {
            id: 2,
            first_name: 'Barack',
            last_name: 'Obama',
            email: 'obama@yahoo.com',
            country: 'United States of America (the)'
        },
        {
            id: 3,
            first_name: 'Angela',
            last_name: 'Merkel',
            email: 'merkel@outlook.com',
            country: 'Germany'
        },
        {
            id: 4,
            first_name: 'Sven',
            last_name: 'Jacobsen',
            email: 's.jabobsen@gmail.fi',
            country: 'Finland'
        }
    ];

    ContactModel.Initialize(contacts);
    return ContactModel;
});