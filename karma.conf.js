module.exports = function(config) {
    config.set({
        frameworks: ['jasmine'],

        files: [
            // libraries
            'vendors/jquery/dist/jquery.min.js',
            'vendors/angular/angular.min.js',
            'vendors/angular-route/angular-route.min.js',
            'vendors/ngstorage/ngStorage.min.js',
            'vendors/angular-messages/angular-messages.min.js',
            'vendors/angular-country-picker/country-picker.min.js',
            'vendors/angular-animate/angular-animate.min.js',
            'vendors/angular-bootstrap/ui-bootstrap-tpls.min.js',
            'vendors/angular-mocks/angular-mocks.js',
            'scripts/app.js',

            // models
            'scripts/models/*.js',

            // controllers
            'scripts/controllers/*.js',

            // tests
            'tests/*.js'

        ]
    })
}